/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/

function user1() {
    let yourName = prompt("Enter  your Name:");
    let yourAge = prompt("Enter your age:");
    let yourAdd = prompt("Enter your Address:");

    console.log("Hello, " + yourName, "you are," + yourAge + "of age", "you live in, " + yourAdd);
}

user1();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

function dispArtist() {
    let favArtist = [
        "Joni Mitchell",
        "John Mayer",
        "Lifehouse",
        "Bruno Mars",
        "Michelle Branch",
    ];

    console.log("My top Music Artist: " + favArtist);
}


dispArtist();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

function moviesList() {
    let myMovies = [
        "The Intern - 59% Tomatometer",
        "Mr. Church - 24% Tomatometer",
        "Notting Hill - 83% Tomatometer",
        "When Harry Met Sally - 91% Tomatometer",
        "The Wailing - 99% Tomatometer",
    ];

    console.log("My top Movies: " + myMovies);
}


moviesList();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/




let printFriends = function printUsers() {
    alert("Hi! Please add the names of your friends.");
    let friend1 = prompt("Enter your first friend's name:");
    let friend2 = prompt("Enter your second friend's name:");
    let friend3 = prompt("Enter your third friend's name:");

    console.log("You are friends with:")
    console.log(friend1);
    console.log(friend2);
    console.log(friend3);
};

printFriends()

// console.log(friend1);
// console.log(friend2);